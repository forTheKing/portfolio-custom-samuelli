## Samuel Li's Portfolio Site

### Under the hood
- Gatsby Starter
  - Using GraphQL
  - React
- TailwindCSS
- Typescript

### Goals

1. Headless CMS that consumes mark down files with frontmatter
   - This is to ensure that the next iteration of my Portfolio I never again have to rewrite content, and makes writing content faster for the web.  Also GraphQL has been incredibly helpful in organizing content.

2. Component driven
   - Using React create various reusable and modular components for easier maintenance and future-proofing.

3. Bespoke
   - While starting from the Gatsby Starter project, integrate and satisfy my appetite to learn by creating components from scratch and implement a style that is representative of me.

4. Use utility driven CSS vs bootstrap
   - Previous portfolio sites I made used bootstrap, but opted to go for tailwind because of ease of customizability and also I found it simpler to make minute changes to the UI in a component setting.