/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.com/docs/gatsby-config/
 */

module.exports = {
  siteMetadata: {
    title: `Samuel Li Toronto | UI/UX Designer & Front End Developer`,
    siteUrl: `https://www.samuelli.ca`,
    author: `Samuel Li`,
    description: `A curious developer and designer who champions holistic and strategic approaches to code and design.`,
    keywords: `Samuel Li, Front End Developer, Toronto, UI Designer, Portfolio, React, Gatsby`
    
  },
  plugins: [
    `gatsby-plugin-postcss`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `src`,
        path: `${__dirname}/src/`,
      },
    },
    {
      resolve: 'gatsby-plugin-web-font-loader',
      options: {
        google: {
          families: ['Nunito Sans:400,600,700,800']
        }
      }
    },
    {
      resolve: `gatsby-plugin-sass`,
      options: {
          implementation: require('sass'),
          postCssPlugins: [require('tailwindcss')]
      },
    },
    `gatsby-transformer-remark`,
    `gatsby-plugin-emotion`,
    `gatsby-plugin-lodash`,
    `gatsby-plugin-react-helmet`
  ],
}

