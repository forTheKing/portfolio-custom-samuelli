import React, {Component} from "react";
import styles from "./imac-graphic.module.scss";

export default class IpadGraphic extends React.Component {
    render(){
        return (
            <div className={styles.cbody}>
                <div className={styles.cmain}>
                    <div className={styles.ccontentwrap}>
                        <div className={styles.ccontent}>
                        </div>
                    </div>
                    <div className={styles.cbottom}>
                    </div>
                    <div className={styles.cleg}>
                    </div>
                    <div className={styles.cfootwrap}>
                        <div className={styles.cfoot}>
                        </div>
                    </div>
                </div>
          </div>
        )
    }
}