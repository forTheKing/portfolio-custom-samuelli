/*
query {
    allMarkdownRemark {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            date
            url
          }
          fields {
            slug
          }
          excerpt
        }
      }
    }
*/

interface MarkdownNode {
    readonly id: string;
    readonly fields: MarkdownNodeFields;
    readonly frontmatter: MarkdownNodeFrontmatter;
    readonly excerpt: string;
}

interface MarkdownNodeFields {
    readonly slug: string;
}


interface MarkdownNodeFrontmatter {
    readonly url: string;
    readonly codeUrl: string;
    readonly title: string;
    readonly date: string;
    readonly tagsUI: Array<String>;
    readonly tagsDev: Array<String>;
    readonly previewPhone: string;
    readonly previewTablet: string;
    readonly previewDesktop: string;
    readonly previewVideo: string;
    readonly summary: string; 
    readonly subtitle: string;
}


export {
    MarkdownNode,
    MarkdownNodeFields,
    MarkdownNodeFrontmatter
}
