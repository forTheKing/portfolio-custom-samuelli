export enum HeroStates {
    start = "START",
    isPresenting = "PRESENTING",
    hidden = "HIDDEN"
}