export enum LabelAppearances {
    Primary = "primary",
    Secondary = "secondary",
    Tertiary = "tertiary"
}