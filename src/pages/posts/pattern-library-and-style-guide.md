---
title: Pattern Library and Style Guide
subtitle: Node Based Web Project - Front End Developer
tagsDev: 
    - NodeJS
    - Fabricator
    - Markdown
    - Git
    - Sass
    - Handlebars
    - Gulp
summary: Migrate existing brand guidelines and pattern library to node based style guide and train team members on contribution process.
order: 6
---

The pattern library is used by members of the Digital Marketing Team, both technical and non-technical, to reference branding and usability patterns.

The existing version needed to be migrated and enhanced to a more robust, easy to manage system that allowed Designers and UX Strategists to contribute actively.

The Pattern library allowed our development and marketing teams to be able to reference all approved patterns and became the golden source for any content slated to launch on TI.com.


## Requirements

- Migrate content into the system based on `Fabricator` (a `NodeJS` based framework)
- Define the Contribution model and train the team on how to submit revisions for approval using `Git`
- Create and implement templates using Handlebars to meet requirements for documentation
- Integrate the core `SASS` repo into the Pattern Library project to allow leverage of the same patterns used on Production
- Create a series of written guides and screencasts to train designers and UX strategists on how to contribute


## Challenge 1 - Customizing Fabricator to meet requirements

Fabricator out of the box offered the ability to pass in Markdown files and churn out the HTML and associated navigation in a template suited for UI Toolkits/Pattern Libraries.

This NodeJS based tool supported two levels of navigation, but our requirements needed three levels for the side-navigation, with Level 2 Items being expandable and collapsible.

Example:
<ul>
    <li>
        Level 1
        <ul>
            <li>
                Level 2
                <ul>
                    <li>Level 3a</li>
                    <li>Level 3b</li>
                </ul>
            </li>
            <li>Level 2a</li>
        </ul>
    </li>
    <li>Level 1a</li>
    <li>Level 1b</li>
</ul>

### Solution

I customized Fabricator to output a three-level interactive navigation using `Handlebars`, `Javascript`, and `SASS`. I combed through the Fabricator documentation to leverage the method they were using to read the file structure and then customized the template. The end-users could drag and drop their `markdown` files and organize them using the folder-structure on their computers.

I designed and implemented a UI for the navigation item to expand and collapse. It was visually represented and did not compete with the hierarchy and emphasis on the page.


### Result

The pattern library now had the ability to automatically output a `side-nav` with three levels that could be collapsed and expanded. The solution enabled end-users to modify the folder structure and filenames in their directory to achieve the desired content strategy. Utilizing as much of the Fabricator code and intended structure as possible enabled us to ensure we had a more maintainable code-base that wasn't bug-prone.

## Challenge 2 - Training and securing adoption

* Team members across three different sites (California, Toronto and Dallas) had varying technical aptitudes
* Adoption hinged on the ability for a team to work and contribute independently 

This was the first time we were introducing Git, NodeJS, NPM and Markdown to our team. Most of our team members were design-focused, and most members did not have experience working with a versioning system like Git.

### Solution

I wrote a detailed tutorial and walkthrough on contributing the first pattern to the Pattern Library. I then created narrated screencasts and led multiple live deep-dive sessions. I supported our team with one-on-one calls and meetups and started seeing the team pick up. Eventually, the team utilized Git (version control) to contribute with write-ups in markdown and integrated into Fabricator.

The last step was to document for our Admins how they could review, approve or reject a submission and publish to our server.


## Result

Ultimately, I witnessed our team grow technically, understanding version control, and became actively involved in contributing patterns and documentation for our online style guide and pattern library.  The Pattern Library that was accessible to our development and marketing departments served as the golden source for all UI patterns and as a Style Guide allowing all of TI.com to be consistent and speak the same design language.
