---
title: "Server Rendered Product Page"
tagsDev:
    - Spring Boot 2
    - Thymeleaf
    - Web Components
    - StencilJS
    - Typescript
    - JSX
    - SASS
    - JSOUP
    - JUNIT testing
    - Lighthouse
    - Metrics
tagsUI:
    - SEO
    - Localization
url: "https://www.ti.com/product/OPA333"
previewDesktop: productDesktop.jpg
subtitle: Java Based Web App - Front End Developer
summary: Rehaul the Tabbed based Product Pages to a single page experience using a new framework and onboard other front-end designers to contribute.
order: 4
---

The Product Folders were rehauled from a UX Perspective going from a multi-tab to a single-page experience with a new look and feel.  From a development standpoint we also reworked the services and moved to a new technology stack on the front-end to make our project more agile and involve more Design resources to make code changes.

This was a large project requiring close collaboration with the back-end team as well as our UX Design team.

As a project it was complex because TI has a portfolio of over 80,000 products, each with its own associated specifications, training materials, as well as primary and secondary associated parts.

#### Before

![Before changes](/FA-en.jpg "Tabbed Product Folders - before")

#### After

![After changes](/productDesktop.jpg "Tabbed Product Folders - before")

View an example page: [here](https://www.ti.com/product/OPA333)

## Role

As a front-end developer, I contributed production code to implement the UI based on the business logic and UX requirements. I helped to wire up the controller with the service using `Springboot` and worked closely with the back-end team to coordinate when business logic was appropriate on the front-end or the back-end. I helped segment the initial `Thymeleaf` into fragments, broken out logically depending on the UI requirements.

I contributed to many reusable web components utilized across the page and targeted for other applications.  Since each component is meant for use globally, it went through an approval process across, business, development, and design.

## My contributions to requirements

* Localization for Chinese and Japanese
* Work with Metrics team and implement metrics tracking (GA)
* Conform UI to Search engine optimization initiatives
* Meet TI branding and guidelines
* Contribute to reusable web components
* Applied business logic requirements on Thymeleaf templates
* Integrated unit tests
* Supported by IE11, Firefox and Chrome
* Build out base HTML connected to JSON data from services provided by another team
* Write integration tests using JSOUP and JUnit
* Contributed to 8 patterns/web components providing SASS for shadow DOM

## Example of deliverables

### Implementation of the `sticky-chapter-nav`

View live: [here](https://www.ti.com/product/INA121)

The `sticky-chapter-nav` is a web component I worked on that required animating into view when sticky is activated.   I utilized CSS transitions in conjunction with the component’s `@state` in order to achieve the effect.  

A challenge came with the requirement of supporting IE11, where I needed to make adjustments to the `scrollTo` function in order for IE11 to work correctly.

In addition, one of the design requirements is that there is a header above the `sticky-chapter-nav` that varies in content depending if the product is available for Order.  By using `slots` on `StencilJS` we were able to control that content and keep business logic on the `Thymeleaf` layer.

Another challenge was correctly updating which link is active based on when the customer has scrolled past the anchor.  I needed to work with the UX Team in order to define when exactly the link and make those changes to the code.

#### Running into bugs

One bug that was discovered during testing was that in some cases, the `sticky-chapter-nav` would be missing links.  

**Web Component race conditions**
I encountered a race condition as I was tasked to fix a bug that caused the `sticky-chapter-nav` to not populate with all of its chapter-nav links correctly.

#### Isolating the issue

Using Chrome Dev Tools with a throttled network and breaking on the render code for the web component, I isolated the moment the `sticky-chapter-nav` was querying for `chapter-nav-anchors` on the page and populating them as links. I determined that the `chapter-nav-anchors` did not exist in the DOM when the `sticky-chapter-nav` was rendered, given the asynchronous nature of how StencilJS web components are rendered/hydrated as the DOM is completing its page load.

The `sticky-chapter-nav` was querying for the `chapter-nav-anchors` but during slow page loads, the `sticky-chapter-nav` would render first before the `chapter-nav-anchors` is on the DOM, causing the `sticky-chapter-nav` to fail in populating the link.


#### Solution
The solution we ended up implementing was to listen for the document page load event during its component lifecycle when it is finished loading. This ensured that the DOM completely rendered before looking for the `chapter-nav-anchors`.
