---
title: "Technical Document Reader"
subtitle: Responsive Web App - UI Designer & Front End developer
tagsDev: 
    - NodeJS
    - RiotJS
    - SASS
    - Javascript
    - HTML5
    - MaterializeCSS
tagsUI: 
    - UI/UX
    - Usability testing
    - Responsive design
    - Prototyping
    - Mock ups
    - Motion design
url: https://www.ti.com/document-viewer/OPA333/datasheet
previewPhone: techdocreader-phone.jpg
previewTablet: techdocreader-tablet.jpg
order: 5
---

The Technical Document reader is a web application in the semiconductor space allowing the engineer to view technical documents on their mobile or desktop device. This was a NodeJS based product that allowed each segment of the document's xml to be lazy loaded in with various reader settings for the customer.

### Role

UI Designer and Front End Developer

### Key contributions

- Guide the team to integrate responsive design and leading the decison on a responsive ready front-end framework
- Innovate by creating and utilizing an automated screenshot tool to test CSS changes across all devices and screens
- Partner closely with Senior UX to conduct user interviews and vet out usability solutions
- Used Prototyping and Mockup tools to get sign-off for key parts of the Reader such as the Table of Contents
- Continued to maintain product by integrating new features post-launch through development on the client side