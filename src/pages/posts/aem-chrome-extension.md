---
title: Digital Assets Manager Meta Data autofiller
subtitle: Chrome extension - Front End Developer
tagsDev: 
    - Javascript
    - JSON
    - HTML5
    - Chrome
summary: During a hackathon type initiative, created a well received Chrome extension to save our Digital Graphic Designers on average 10 minutes of manual data entry per image when uploading images to the Asset Management System.
previewDesktop: chromeextension.png
order: 7
---

## Background

With batches of images coming through the pipeline that needed approval and upload into our Digital Asset Management system, our Digital Image Designers would need to manually fill in Metadata fields in order for assets to be organized (in some cases up to 32 fields manually), copy and pasting from their Jira Ticket as well as filling in the fields based on what the image is.

This was a solution I came up with to assist our team that would automatically copy the appropriate fields and apply a templated meta data based on what the image is.  So if it is a Square Chipshot, it would fill in the metadata tags based on that criteria and pattern.

![Chrome Extension](/chromeextension.png)

## Initial Data on Time-savings/Effort

The data is for AEM DAM image approvers who have to fill in metadata tags for both source and production images as was the requirement.

The time measured is just for the inputting of metadata information.
Program: Mousotron (click tracker)

For a Rectangle Chipshot without Autofiller:

- Avg Time: 12min 13s 
- Keystrokes: 159
- Clicks: 185

Rectangular Chipshot with Autofiller

- Time: 2min 23s
- Keystrokes: 80
- Clicks: 80

## Reception

Our Digital Image Designers ended up using the extension as one of their suite of tools as part of the image upload process.  Over the years there has been about 4 feature requests from the users.  Since many times these images came in batches it became a product that ended up having feature requests as well as update requests to align with new metadata fields.

## Process

The metadata included details that are important such as image aspect ratio, content author, and even things like indicating the pictures backdrop.

I ended up creating methods where it would simply do a query on the Jira page of values that I put in a JSON file so it can be easily modified when fields are updated.

Since there was also manual entries that needed to be done as well, I captured the patterns and introduced a convenience function of selecting what type the image is to further reduce the number of fields the Image Designer had to input.

After getting some enthusiasm from my Manager, I wrote out instructions on how to install and use it, eventually adding hot-keys as well and onboarded our Digital Image Designers on how to use it.