---
title: Responsive Hardware Project & Improving Front-end process
date: "2020"
subtitle: Responsive web app - Front end designer lead
tagsDev:
    - Thymeleaf
    - Web Components
    - StencilJS
    - Typescript
    - JSX
    - SASS
    - Lighthouse
tagsUI: 
    - Responsive
previewPhone: tools-phone.jpg
previewTablet: tools-tablet.jpg
url: "https://www.ti.com/product/OPA333"
summary: Taking on a leadership role while bringing from concept to development a component based, responsively designed, server-rendered page enabling customers to access TI's vast hardware portfolio and meet our ecommerce goals.
order: 3
---

This project involved multiple departments in integrating the newly recreated hardware pages to the new architecture established in the previous [Product page launch](/posts/product-folder-evolution.md). The main point of focus was to integrate responsive design into the page template, create new responsive UI web components and convert existing components to be responsive.

In addition to developing the UI for the Responsive Hardware Web Application project, I was tasked to help lead, build up and establish our front-end process.

The page is live on production now and is viewable [here](https://www.ti.com/tool/LAUNCHXL-CC26X2R1).

**Mobile**
<img alt="Mobile tool folders" src="/tools-phone.jpg" style="height:300px"/>

**Tablet**
<img alt="Tablet tool folders" src="/tools-tablet.jpg" style="height:500px"/>

**Desktop**
![Mobile tool folders](/tools-desktop.jpg)

<h2 id="leadership">Taking on the leadership challenge</h2>

I was given the opportunity to be a team lead to improve the process for our code contribution, and build a stronger bridge between design and development.

## Initiatives

* Establish accountability for Designers and Developers to meet before implementation using tools within Confluence
* Unified our development projects with our Design System by integrating our design foundations as variables shared across all Web Components, UXPIN and Global CSS. This ensured  consistency across TI.com and saved time and resources
* Start Front-end design team bi-weekly meetups to break silos between our team members across three locations and collaborate
* Acted as the ambassador for Web Components, providing live training sessions for Designers and the UX Team. This encouraged Designers to think on a component level when designing their comps

## Problem #1 
**Developers and Designers were in silos**

I recognized we needed accountability and visibility to ensure that developers weren't going straight to development without consultation with the UI/UX Designers. By establishing a method of visibility for our Design Team we could ensure that designers and developers were aligned.

### Solution
- Utilized Confluence to bring visibility and accountability to our Design leaders and to capture the status of if/when the developer has met with the designer(s)
- Weekly meetup with our Development Lead and Creative Strategist as a forum to bring up UX Issues during development

### Result

As a result, we were able to decrease development time. Key UX decisions that surfaced could be addressed quickly now that links were made between developers and designers.

## Problem #2 Feedback from Creative Director was being lost

Our Organizational structure meant our Design team was in a different department from our Development team. Both resources were coming together in a project-based team structure. This meant that our design feedback loop was new and unfamiliar to our team members. We found that as we moved from Proof of Concept phase to Development, we were losing feedback from our Creative Director. This meant lost time and effort as the development team needed to sift through Jira comments and our Creative Director needed to rewrite her feedback.

We were losing this feedback because the feedback and UI/UX concerns brought up needed further exploration or went beyond the scope of that particular "story". With the original agreement that the UX issue would be handled moving forwards, the Creative Specialist would move the story forward. However we found that there were cases where the developer/designer would not come back and take action.

### Solution 
- Established our UI/UX review process through updating the JIRA workflow, ensuring the story was created in the backlog and captured anything that was out of scope or required further exploration 
- Ensured our Front-End Designers took the lead in confirming that every UI Story they were assigned had an approved Review subtask before progressing their status
- Ensured our Front-End Designers took the lead in confirming that every UI Story they were assigned had an approved Review subtask before progressing their status

### Result
- The team has adopted the new UI/UX Review process 
- Feedback that is not implemented in the story is captured as an actionable item in the backlog
