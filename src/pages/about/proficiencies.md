---
title: Proficiencies
aboutsection: true
url: ""
summary: I have a passion for all stages of Front End Development...
---

As a Front End Developer, I've actively contributed to the code base of forward and back facing Angular Web Applications, Responsive web projects, Node based projects and our Web Component UI library, closing out any implementation gaps involving design and UX.