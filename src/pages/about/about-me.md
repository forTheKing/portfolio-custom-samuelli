---
title: "About me"
aboutsection: true
summary: I have a passion for all stages of Front End Development...
---
I'm passionate for all stages of Front End Development and UI/UX Design. I love learning about new web technologies and thrive with component driven development and working with design systems.

In my current role I am helping to lead our front end design team by helping to give guidance on code standards, web component philosophy, while contributing to front-end development and UI/UX decisions to complete projects.

I've been involved in multiple roles as a UI/UX Design lead and Front End Developer for back-facing and customer-facing projects.  My unique hybrid of core skills, background and toolset has allowed me to contribute code and design, as well as support our team in building bridges between the Design and Development departments.